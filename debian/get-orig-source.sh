#!/bin/bash

# Repack autofdo with its git submodules.

set -ex

die()
{
    printf "E: %s\n" "$*" > /dev/stderr
    exit 1
}

[ ! -d debian/ ] && die "'debian' directory not found."
[ "$1" != "--upstream-version" ] && die "This script should be invoked by uscan."
[ -z "$2" ] && die "You must provide the upstream version to be repacked."

readonly DS_VERSION="$2"
readonly UPSTREAM_VERSION=$(printf "%s" "${DS_VERSION}" | sed 's@+ds1@@g')
readonly PROJECT_DIR="google-autofdo-${UPSTREAM_VERSION}"
readonly ORIG_TAR=$(readlink -f "../autofdo_${DS_VERSION}.orig.tar.gz")

readonly WORKDIR=$(mktemp -d)
trap 'rm -rf "${WORKDIR}"' EXIT INT

readonly SUBMODULES="perf_data_converter glog abseil"

tar xf "${ORIG_TAR}" -C "${WORKDIR}"
cd "${WORKDIR}"

if [ ! -d "${PROJECT_DIR}" ]; then
    find . -maxdepth 1 -mindepth 1 -type d -name "google-autofdo*" \
	 -exec mv '{}' "${PROJECT_DIR}" ';'
fi

git clone https://github.com/google/autofdo
cd autofdo
(git checkout "v${UPSTREAM_VERSION}" || git checkout "${UPSTREAM_VERSION}") \
    || die "Could not find git tag 'v?${UPSTREAM_VERSION}'."
for sm in ${SUBMODULES}; do
    d="third_party/${sm}"
    projd="${WORKDIR}/${PROJECT_DIR}/${d}"

    git submodule update --init -- "${d}"
    pushd "${d}"
    git archive \
	--format=tar \
	HEAD | (cd "${projd}" && tar xf -)
    popd
done

cd "${WORKDIR}"
tar czf "${ORIG_TAR}" "${PROJECT_DIR}"
